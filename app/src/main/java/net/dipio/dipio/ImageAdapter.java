package net.dipio.dipio;

import android.content.Context;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Adapter for favorite fragment gridView
 */
public class ImageAdapter extends BaseAdapter{

    private Context context;
    /*private List<Pair<String, List<IContacts>>> favoriteList;

    public ImageAdapter(Context context, List<Pair<String, List<IContacts>>> favoriteList){
        this.context = context;
        this.favoriteList = favoriteList;
    }*/
    private List<IContacts> favoriteList;
    public ImageAdapter(Context context, List<IContacts> favoriteList){
        this.context = context;
        this.favoriteList = favoriteList;
    }


    @Override
    public int getCount() {
        return favoriteList.size();
    }

    @Override
    public Object getItem(int position) {
        return favoriteList.get(position);
    }

    @Override
    public long getItemId(int position) {
        //NAP
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        /*ImageView imageView;*/
        View view;
        if (convertView == null){
            // if it's not recycled, initialize some attributes
     /*       imageView = new ImageView(context);
            imageView.setLayoutParams(new GridView.LayoutParams(85,85));
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            imageView.setPadding(8,8,8,8);*/

            view = new View(context);
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.circle_image, parent, false);
        }else {
            /*imageView = (ImageView) convertView;*/
            view =  convertView;
        }

        CircleImageView circleImage= (CircleImageView) view.findViewById(R.id.imgv_image_grid);
        CircleImageView circleConnected = (CircleImageView) view.findViewById(R.id.connected_circle);
        TextView tv = (TextView) view.findViewById(R.id.tv_circle_image);

        IContacts contact = (IContacts) (favoriteList.get(position));
        if (contact.getClass().getSimpleName().compareTo(PersonContact.TAG) == 0){
            //the contact is a friend contact
            if (((PersonContact)contact).getIsFavorite() != -1){
                if (((PersonContact)contact).getLastSeen() == -1){
                    //connected
                    //inflate friend + connected layout
                    circleImage.setImageResource(R.drawable.user_favorite);
                    circleConnected.setImageResource(R.drawable.green_circle_favorite);


                }else {
                    //inflate friend + NOT connected layout
                    circleImage.setImageResource(R.drawable.user_favorite);
                    circleConnected.setImageResource(R.drawable.gray_circle_favorite);
                }
                tv.setText(((PersonContact)favoriteList.get(position)).getDisplayName());

            }

        }else {
            //the contact is a group contact
            if (((GroupContact)contact).getIsFavorite() != -1){
                if (((GroupContact)contact).getLastActive() == -1){
                    //connected
                    //inflate group + connected layout
                    circleImage.setImageResource(R.drawable.group_favorite);
                    circleConnected.setImageResource(R.drawable.green_circle_favorite);

                }else {
                    circleImage.setImageResource(R.drawable.user_favorite);
                    circleConnected.setImageResource(R.drawable.gray_circle_favorite);
                }
                tv.setText(((GroupContact)favoriteList.get(position)).getDisplayName());

            }
        }


        return view;
    }
}
