package net.dipio.dipio;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ContactsFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ContactsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ContactsFragment extends Fragment {

    CollapsingToolbarLayout collapsingToolbar;
    ContactsListView contactsListView;
    MyContactsAdapter adapter;
    View v;
    private ImageView imgvFavorite;
    private ImageView imgvChannel;
    private ImageView imgvSetup;


    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public ContactsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ContactsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ContactsFragment newInstance(String param1, String param2) {
        ContactsFragment fragment = new ContactsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }



    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_contacts, container, false);

      /*  collapsingToolbar =
                (CollapsingToolbarLayout) v.findViewById(R.id.collapsing_toolbar);*/
        contactsListView = (ContactsListView) v.findViewById(R.id.contactsListView);
        imgvFavorite = (ImageView) v.findViewById(R.id.imgv_favorite_bBar);
        imgvChannel = (ImageView) v.findViewById(R.id.imgv_channel_bBar);
        imgvSetup = (ImageView) v.findViewById(R.id.imgv_setup_bBar);
        imgvFavorite.setOnClickListener(buttomBarListener);
        imgvChannel.setOnClickListener(buttomBarListener);
        imgvSetup.setOnClickListener(buttomBarListener);
        return v;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        contactsListView.setPinnedHeaderView(LayoutInflater.from(getActivity()).inflate(R.layout.item_contacts_header, contactsListView, false));
        contactsListView.setAdapter(adapter = new MyContactsAdapter());


    }

    class MyContactsAdapter extends ContactsListViewAdapter {
        List<Pair<String, List<IContacts>>> all = ContactsData.getAllData();

        @Override
        public int getCount() {
            int res = 0;
            for (int i = 0; i < all.size(); i++) {
                res += all.get(i).second.size();
            }
            return res;
        }

        @Override
        public IContacts getItem(int position) {
            int c = 0;
            for (int i = 0; i < all.size(); i++) {
                if (position >= c && position < c + all.get(i).second.size()) {
                    return all.get(i).second.get(position - c);
                }
                c += all.get(i).second.size();
            }
            return null;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        protected void onNextPageRequested(int page) {
        }

        @Override
        protected void bindSectionHeader(View view, int position, boolean displaySectionHeader) {
            if (displaySectionHeader) {
                view.findViewById(R.id.header).setVisibility(View.VISIBLE);
                TextView lSectionTitle = (TextView) view.findViewById(R.id.header);
                lSectionTitle.setText(getSections()[getSectionForPosition(position)]);
            } else {
                view.findViewById(R.id.header).setVisibility(View.GONE);
            }
        }

        @Override
        public View getContactsView(int position, View convertView, ViewGroup parent) {
            View res = convertView;
            if (res == null) res = getActivity().getLayoutInflater().inflate(R.layout.contacts_list_item, null);

            ImageView imgvFavorite = (ImageView) res.findViewById(R.id.imgv_favorite_contacts);
            TextView tvUserName = (TextView) res.findViewById(R.id.tv_contactName_contacts);
            ImageView imgvBubble = (ImageView) res.findViewById(R.id.imgv_bubble_contacts);
            ImageView imgvWalki = (ImageView) res.findViewById(R.id.imgv_ptt_contacts);


            IContacts contact = getItem(position);
            Log.i("!!!!!!!!!!!!!!!!!",contact.getClass().getSimpleName());

            if (contact.getClass().getSimpleName().compareTo(PersonContact.TAG) == 0){
                if(((PersonContact)contact).getIsFavorite() == -1){
                    //not favorite
                    imgvFavorite.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.icone_star_empty, null));
                }else {
                    imgvFavorite.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.icon_filled_star, null));
                }
                tvUserName.setText(((PersonContact)contact).getDisplayName());

            }else {
                if(((GroupContact)contact).getIsFavorite() == -1){
                    //not favorite
                    imgvFavorite.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.icone_star_empty, null));
                }else {
                    imgvFavorite.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.icon_filled_star, null));
                }
                tvUserName.setText(((GroupContact)contact).getDisplayName());

            }



            imgvBubble.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.icon_bubble, null));
            imgvWalki.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.walki_talki, null));


            return res;
        }

        @Override
        public void configurePinnedHeader(View header, int position, int alpha) {
            TextView lSectionHeader = (TextView)header;
            lSectionHeader.setText(getSections()[getSectionForPosition(position)]);
           /* lSectionHeader.setBackgroundColor(alpha << 24 | (0xbbffbb));
            lSectionHeader.setTextColor(alpha << 24 | (0x000000));*/
        }

        @Override
        public int getPositionForSection(int section) {
            if (section < 0) section = 0;
            if (section >= all.size()) section = all.size() - 1;
            int c = 0;
            for (int i = 0; i < all.size(); i++) {
                if (section == i) {
                    return c;
                }
                c += all.get(i).second.size();
            }
            return 0;
        }

        @Override
        public int getSectionForPosition(int position) {
            int c = 0;
            for (int i = 0; i < all.size(); i++) {
                if (position >= c && position < c + all.get(i).second.size()) {
                    return i;
                }
                c += all.get(i).second.size();
            }
            return -1;
        }

        @Override
        public String[] getSections() {
            String[] res = new String[all.size()];
            for (int i = 0; i < all.size(); i++) {
                res[i] = all.get(i).first;
            }
            return res;
        }

    }










    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onResume() {
        super.onResume();
        /*collapsingToolbar.setTitle("Title");*/
        getActivity().setTitle("IContacts");

    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        void switchFragment(int imageId);
        void onFragmentInteraction(Uri uri);
    }

    View.OnClickListener buttomBarListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            mListener.switchFragment(v.getId());
        }
    };

}

