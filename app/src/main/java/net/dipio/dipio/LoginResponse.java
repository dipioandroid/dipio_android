package net.dipio.dipio;

import java.net.InetAddress;

/**
 * Created by qwerty on 04/08/16.
 */
public class LoginResponse {

    private int returnCode;
    private String errorMsg;
    private String sId;
    private String dssSvr;


    public LoginResponse(int returnCode, String errorMsg, String sId, String dssSvr){
        this.returnCode = returnCode;
        this.errorMsg = errorMsg;
        this.sId = sId;
        this.dssSvr = dssSvr;

    }
}
