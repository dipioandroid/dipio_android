package net.dipio.dipio;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.JsonRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URL;
import java.net.URLDecoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.Permission;
import java.util.Locale;

public class LoginActivity extends AppCompatActivity {

    private static String[] PERMISSION_READ_PHONE_STATE = {Manifest.permission.READ_PHONE_STATE};
    private static final int REQUEST_PHONE_STATE = 0;
    private static final int RETURN_CODE_SUCCESS = 0;
    private static final int RETURN_CODE_FAILURE = -1;

    //data info server
    private static final String DIS = "//http:dipio.net";

    private TextInputLayout userWrapperLayout;
    private TextInputLayout passWrapperLayout;
    private TextInputLayout companyWrapperLayout;
    private TextInputEditText userEditText;
    private TextInputEditText passEditText;
    private TextInputEditText companyEditText;

    private Button btnLogin;
    private View mainLayout;

    private String company;
    private String user;
    private String password;

    private ProgressBar progressBar;
    private RequestQueue requestQueue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        companyWrapperLayout = (TextInputLayout) findViewById(R.id.wrapper_et_company_login);
        userWrapperLayout = (TextInputLayout) findViewById(R.id.wrapper_et_user_login);
        passWrapperLayout = (TextInputLayout) findViewById(R.id.wrapper_et_pass_login);
        userEditText = (TextInputEditText) findViewById(R.id.et_user_login);
        companyEditText = (TextInputEditText) findViewById(R.id.et_company_login);
        passEditText = (TextInputEditText) findViewById(R.id.et_pass_login);

        btnLogin = (Button) findViewById(R.id.btn_login_loginActivity);
        mainLayout = findViewById(R.id.mainLayout_login);


        progressBar = (ProgressBar) findViewById(R.id.progressBarId);
        progressBar.setVisibility(View.GONE);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //check internet connectivity
       /*         if(!isInternetConnected()){
                    Toast.makeText(LoginActivity.this, R.string.no_internet_message, Toast.LENGTH_SHORT).show();

                }else {
                     company = companyEditText.getText().toString();
                     user = userEditText.getText().toString();
                     password = passEditText.getText().toString();

                    //check that input isnt enpty
                    if (!checkInputValidity(company, user, password)){
                        //textview will show error message
                        //nothing to do here
                    }else {
                        //encrypt password
                        createUserProfile(company, user, password);


                    }
                }*/

                //TODO - remove this part : its for DEBUG and uncomment the part above!!!!!
                startActivity(new Intent(LoginActivity.this, MainActivity.class));
                overridePendingTransition(R.anim.push_up_in, R.anim.push_up_out);
                finish();
            }
        });

    }


   private boolean checkInputValidity(String company, String user, String password){

       if (TextUtils.isEmpty(company)){
           companyEditText.setError(getResources().getString(R.string.emptyField));
       }else if (TextUtils.isEmpty(user)){
           userEditText.setError(getResources().getString(R.string.emptyField));
       }else if (TextUtils.isEmpty(password)){
           passEditText.setError(getResources().getString(R.string.emptyField));
       }else {
           companyWrapperLayout.setErrorEnabled(false);
           userWrapperLayout.setErrorEnabled(false);
           passWrapperLayout.setErrorEnabled(false);
           return true;
       }
       return false;
   }


    private boolean isInternetConnected(){
        ConnectivityManager cm =
                (ConnectivityManager)getSystemService(CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null &&
                activeNetwork.isConnected();
    }

    private String encryptPassword(String company, String user, String password){

        MessageDigest md = null;
        String strPass = company + "_" + user + "_" + password;
        byte[] data = null;
        try {
            md = MessageDigest.getInstance("SHA-256");
            md.update(strPass.getBytes());
            data = md.digest();


        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        String encryptedPassWithNewLine = Base64.encodeToString(data, Base64.URL_SAFE);
        return encryptedPassWithNewLine.substring(0, encryptedPassWithNewLine.length()-1);
    }

    private String retrieveRegionalSettings(){

        Locale locale = Locale.getDefault();
        String lang = locale.getLanguage();
        String country = locale.getCountry();
        Log.i("Lang !!!", lang);
        Log.i("country !!!", country);

        return lang + "-" + country;
    }

    private String retrieveApiVersionName(){

        PackageInfo packageInfo;
        String apiVersion = null;
        try {
            packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            apiVersion = packageInfo.versionName;
            Log.i("apiVErsion !!!", apiVersion);

        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return apiVersion;
    }

/*    private void retrievePhoneNumber(){

        //check READ_PHONE_STATE permission
        if (ActivityCompat.checkSelfPermission(this, PERMISSION_READ_PHONE_STATE[0]) !=
                PackageManager.PERMISSION_GRANTED){
            //no permission available
            requestPhoneStatePermission();

        }else{
            //permission is granted already
            getUserPhoneNumber();
        }

    }*/

    private String getUserPhoneNumber(){

          TelephonyManager telephonyManager = (TelephonyManager)getSystemService(TELEPHONY_SERVICE);
          return telephonyManager.getLine1Number();

    }

    private void requestPhoneStatePermission(){

        if (ActivityCompat.shouldShowRequestPermissionRationale(this, PERMISSION_READ_PHONE_STATE[0])){
            //add rational for permission
            Snackbar.make(mainLayout, R.string.phone_state_rational, Snackbar.LENGTH_INDEFINITE).setAction(R.string.ok, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ActivityCompat.requestPermissions(LoginActivity.this, PERMISSION_READ_PHONE_STATE, REQUEST_PHONE_STATE);
                }
            }).show();

        }else{
            //no permission yet and no rational message
            ActivityCompat.requestPermissions(this, PERMISSION_READ_PHONE_STATE, REQUEST_PHONE_STATE);
        }
    }

    /**
     * Callback received when a permissions request has been completed.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if (requestCode == REQUEST_PHONE_STATE){

            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                //phone status permission granted, can retrieve number
                createUserProfile(company, user, password);

            }else {
                //phone number permission not granted, can ask for phone number
                //TODO need to ask for phone number and create user
            }
        }

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private void createUserProfile(String company, String user, String password){

        String encryptedPassword = encryptPassword(company, user, password);
        String regionalSettings = retrieveRegionalSettings();
        String apiVersion = retrieveApiVersionName();


            //check READ_PHONE_STATE permission
            if (ActivityCompat.checkSelfPermission(this, PERMISSION_READ_PHONE_STATE[0]) !=
                    PackageManager.PERMISSION_GRANTED){
                //no permission available
                requestPhoneStatePermission();

            }else{
                //permission is granted already
                String userPhoneNumber = getUserPhoneNumber();
                if (TextUtils.isEmpty(userPhoneNumber)){
                    userPhoneNumber = null;
                }
                //create user and login in background
                UserDetails newUser = new UserDetails(company, user, encryptedPassword, apiVersion, regionalSettings, userPhoneNumber);
                sendLoginRequestToServer(newUser);
            }


    }

    private void sendLoginRequestToServer(UserDetails userDetails){
        progressBar.setVisibility(View.VISIBLE);

        Gson gson = new GsonBuilder().disableHtmlEscaping().create();
        String jsonString = gson.toJson(userDetails);
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(jsonString);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        requestQueue = Volley.newRequestQueue(LoginActivity.this);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, DIS
                , jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                progressBar.setVisibility(View.GONE);
                try {
                    int responseCode = response.getInt(ConstantsDipio.RETURN_CODE);
                    String errorMsg = response.getString(ConstantsDipio.ERROR_MSG);
                    String sId = response.getString(ConstantsDipio.SID);
                    String dssSvr = response.getString(ConstantsDipio.DSS_SVR);
                    LoginResponse loginResponse = new LoginResponse(responseCode, errorMsg, sId, dssSvr);

                    if (responseCode == RETURN_CODE_SUCCESS){
                        //user login authenticated
                        //write to sharedpreference that user logged in and also write sessionID and dssSvr string
                        SharedPreferences sharedPref = getSharedPreferences(getResources().getString(R.string.loginStatusFile), MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPref.edit();
                        editor.putBoolean(getString(R.string.preference_key_isLoggedIn), true);
                        editor.putString(getString(R.string.sessionID), sId);
                        editor.putString(getString(R.string.dssSvr), dssSvr);
                        editor.commit();

                        startActivity(new Intent(LoginActivity.this, MainActivity.class));
                        overridePendingTransition(R.anim.push_up_in, R.anim.push_up_out);
                        finish();

                    } else {
                        //there is an issue with login credentials
                        Toast.makeText(LoginActivity.this, R.string.wrongCredentials, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VollyErrorLogIn", error.toString());
                progressBar.setVisibility(View.GONE);
                Toast.makeText(LoginActivity.this, R.string.serverConnectionFailed, Toast.LENGTH_SHORT).show();
            }
        });

    }



/*
    //logging in background
    private class LoginBackgroundTask extends AsyncTask<JSONObject, Void, String>{
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
            requestQueue = Volley.newRequestQueue(LoginActivity.this);



        }

        @Override
        protected String doInBackground(JSONObject... params) {
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST,
                    "www.gmail.com", params[0], new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("VollyErrorLogIn", error.toString());
                }
            });
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            progressBar.setVisibility(View.GONE);
        }


    }*/


}
