package net.dipio.dipio;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;

/**
 * Created by qwerty on 14/08/16.
 */
public class ChatAdapter extends BaseAdapter{

    private Context context;
    private boolean isMapShowing = true;
    private View v;
    private  FrameLayout frameLayoutMap;
    private  FrameLayout frameLayoutPic;
    private Button btnDragMap;

    public ChatAdapter(Context context){
        this.context = context;
    }
    @Override
    public int getCount() {
        return 1;
    }

    @Override
    public Object getItem(int position) {
        return 1;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Log.i("getView", "Call get view@@@");

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            v = inflater.inflate(R.layout.chat_buttom_cell, parent, false);
        }else {
            v = convertView;
        }
        frameLayoutMap = (FrameLayout) v.findViewById(R.id.fragment_container_chat_buttom_map);
        frameLayoutPic = (FrameLayout) v.findViewById(R.id.fragment_container_chat_buttom_pic);
        /*btnDragMap = (Button) v.findViewById(R.id.map_transparent_drag_btn);*/
        com.google.android.gms.maps.MapFragment mapFragment = (com.google.android.gms.maps.MapFragment) ((ChatActivity)context).getFragmentManager().findFragmentById(R.id.map_fragment);

        PhotoFragment photoFragment = (PhotoFragment) ((ChatActivity)context).getSupportFragmentManager().findFragmentById(R.id.pic_fragment);
        View fMap = v.findViewById(R.id.map_fragment);

/*
        frameLayoutPic.setVisibility(View.GONE);
*/



    /*    v.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Log.i("ONTOUCH", "ONTOUCH");
                int action = event.getAction();

                switch (action){
                    case MotionEvent.ACTION_DOWN:
                        // Disallow ScrollView to intercept touch events.
                        v.getParent().requestDisallowInterceptTouchEvent(true);
                        break;

                    case MotionEvent.ACTION_UP:
                        // Allow ScrollView to intercept touch events.
                        v.getParent().requestDisallowInterceptTouchEvent(false);
                        break;

                }

                return true;
            }
        });*/

    /*    if (isMapShowing){
            ViewGroup.LayoutParams layoutParams = v.getLayoutParams();
            int[] arr = ((ChatActivity)context).getScreenDimentions();
            layoutParams.height = arr[0];
            v.setLayoutParams(layoutParams);

        }else {
            ViewGroup.LayoutParams layoutParams = v.getLayoutParams();
            layoutParams.height = 200;
            v.setLayoutParams(layoutParams);
        }

        Log.i("GETVIEW", "GETVIEW-----");
        notifyDataSetChanged();*/

        v.setOnTouchListener(viewTouchListener);
        return v;
    }

    public void switchFragmentsVisibility(){

               if (isMapShowing){
                    frameLayoutMap.setVisibility(View.GONE);
                    frameLayoutPic.setVisibility(View.VISIBLE);
                    isMapShowing = false;

                /*    ((ChatActivity)context).setHeaderSize(200, 1);*/
                   ViewGroup.LayoutParams layoutParams = v.getLayoutParams();

                   layoutParams.height = ((ChatActivity)context).dpToPx(200);;
                   v.setLayoutParams(layoutParams);

                }else {
                    frameLayoutPic.setVisibility(View.GONE);
                    frameLayoutMap.setVisibility(View.VISIBLE);
                    isMapShowing = true;

             /*       ((ChatActivity)context).setHeaderSize(200, 2);*/
                   int[] sizes = ((ChatActivity)context).getScreenDimentions();
                   ViewGroup.LayoutParams layoutParams = v.getLayoutParams();
                   int[] t = ((ChatActivity)context).getScreenDimentions();
                   int heightInPx = ((ChatActivity)context).dpToPx(t[0]);
                   Log.i("heightInPx", String.valueOf(heightInPx));
                   layoutParams.height = heightInPx;
                   v.setLayoutParams(layoutParams);

                }
        this.notifyDataSetChanged();

    }

    public boolean getIsMapShowing(){
        return this.isMapShowing;
    }


    View.OnTouchListener viewTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            Log.i("ONTOUCH", "ONTOUCH");
            return false;
        }
    };


/*    @Override
    public boolean onTouch(View v, MotionEvent event) {
        Log.i("ONTOUCH", "ONTOUCH");
        int action = event.getAction();

        switch (action){
            case MotionEvent.ACTION_DOWN:
                // Disallow ScrollView to intercept touch events.

                v.getParent().requestDisallowInterceptTouchEvent(true);
                break;

            case MotionEvent.ACTION_UP:
                // Allow ScrollView to intercept touch events.
                v.getParent().requestDisallowInterceptTouchEvent(false);
                break;

        }

        return true;
    }*/
}
