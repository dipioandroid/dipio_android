package net.dipio.dipio;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.Rect;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.TouchDelegate;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.google.android.gms.maps.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ChatActivity extends AppCompatActivity implements OnMapReadyCallback, MapFragment.OnFragmentInteractionListener, View.OnClickListener, PhotoFragment.OnFragmentInteractionListener, AbsListView.OnScrollListener, View.OnTouchListener {


    /*MapFragment myMApFrag;*/
    private com.google.android.gms.maps.MapFragment mapFragment;
    private PhotoFragment photoFragment;
    GoogleMap map;
    private static boolean isMapShowing = true;
    private FrameLayout framLayoutMap;
    private FrameLayout framLayoutPic;
    private ImageView imgvAddPhoto;
    private PicturesAndGallery picturesAndGallery;
    private View chatRelativeLayout;


    private int lastTopValue = 0;
    private List<String> modelList = new ArrayList<>();
    private ListView listView;
    private ViewGroup header;
    private ChatAdapter adapter;
    private FloatingActionButton fabMap;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        listView = (ListView) findViewById(R.id.list);
        modelList.add("");
        adapter = new ChatAdapter(this);
        listView.setAdapter(adapter);

        LayoutInflater inflater = getLayoutInflater();
        header = (ViewGroup) inflater.inflate(R.layout.chat_top_cell, listView, false);
        listView.addHeaderView(header, null, false);
        fabMap = (FloatingActionButton) findViewById(R.id.fab_map);
        fabMap.setVisibility(View.INVISIBLE);

        listView.setOnScrollListener(this);
        setHeaderSize(260, 0);


        picturesAndGallery = new PicturesAndGallery(this);
        imgvAddPhoto = (ImageView) findViewById(R.id.imgv_add_photo);
        imgvAddPhoto.setOnClickListener(this);


        mapFragment = (com.google.android.gms.maps.MapFragment) getFragmentManager().findFragmentById(R.id.map_fragment);
        photoFragment = (PhotoFragment) getSupportFragmentManager().findFragmentById(R.id.pic_fragment);


        listView.setScrollContainer(false);

        listView.setOnTouchListener(this);

        /*mapFragment.getMapAsync(this);*/


  /*      if (myMApFrag == null || !myMApFrag.isAdded())
            myMApFrag = new MapFragment();

        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction transaction = fm.beginTransaction();
        transaction.replace(R.id.fragment_container_chat_buttom, myMApFrag);
        transaction.commit();
        fm.executePendingTransactions();*/



    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public void chatActivityFragmentSwitcher() {

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.imgv_add_photo:
                adapter.switchFragmentsVisibility();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        //when pressing back when fram with photos is seen, go back to map fragment
        //otherwise, go back to previous activity
        if (!isMapShowing){
            framLayoutPic.setVisibility(View.GONE);
            framLayoutMap.setVisibility(View.VISIBLE);
            isMapShowing = true;
        }else {
            super.onBackPressed();
        }
    }

    public void showSnackBar(){

        Snackbar.make(chatRelativeLayout, R.string.permission_camera_and_storage, Snackbar.LENGTH_INDEFINITE).setAction(R.string.ok, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityCompat.requestPermissions(ChatActivity.this, ConstantsDipio.PERMISSION_STRING_CAMERA_AND_STORAGE, ConstantsDipio.MY_PERMISSIONS_REQUEST_CAMERA_AND_STORAGE);
            }
        }).show();

    }

    public void goToCamera(){
        picturesAndGallery.openCamera();
    }

    public void goToGallery(){
        picturesAndGallery.openGallery();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK){
            if (requestCode == ConstantsDipio.CAMERA_REQUEST){
                //TODO - send the picture to chat via server
                String photoPath = picturesAndGallery.getmCurrentPhotoPath();

            }else if (requestCode == ConstantsDipio.GALLERY_SELECT_IMAGE_REQUEST){
                if (data != null && data.getData() != null){
                    Uri galleryPicUri = data.getData();
                    try {
                        Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), galleryPicUri);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                    //TODO - send the image from gallery to chat via server

            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == ConstantsDipio.MY_PERMISSIONS_REQUEST_CAMERA_AND_STORAGE){
            //permission granted for camera and storage
            if (grantResults.length == 2 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED){
                picturesAndGallery.openCamera();
            }else {
                //permission not granted, show a short snackbar message
                Snackbar.make(chatRelativeLayout, R.string.deny_camera_permission_msg, Snackbar.LENGTH_LONG).show();
            }
        }else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {


    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        Rect rect = new Rect();
        header.getLocalVisibleRect(rect);


    /*

        if (lastTopValue != rect.top) {
*//*            lastTopValue = rect.top;
            header.setY((float) (rect.top /2));*//*
        }
        Log.i("onScrollStateChanged","%^&&*");*/


    }

    public int[] getScreenDimentions(){
       /* DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        int h = displayMetrics.heightPixels;
        int w = displayMetrics.widthPixels;*/

   /*     DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);

        int h = metrics.heightPixels;
        int w = metrics.widthPixels;*/

     Configuration configuration = this.getResources().getConfiguration();
        int screenWidthDp = configuration.screenWidthDp;
        int screenHeightDp = configuration.screenHeightDp;



        return (new int[]{screenHeightDp,screenWidthDp});


    }
    public void setHeaderSize(int buttomHeight, int status){

        if (status == 0) {
            int[] screenDimentions = getScreenDimentions();
            ViewGroup.LayoutParams params = header.getLayoutParams();
       /* params.width = header.getWidth();*/
            params.height = dpToPx(screenDimentions[0] - 200);
            adapter.notifyDataSetChanged();
        }else {
            int[] screenDimentions = getScreenDimentions();
            ViewGroup.LayoutParams params = header.getLayoutParams();
       /* params.width = header.getWidth();*/
            params.height = dpToPx(screenDimentions[0] - 200);

        }



    }

    public int dpToPx(int dp) {
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        int px = Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
        return px;
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        int mEvent = event.getActionMasked();
        Log.i("event",String.valueOf(event.getAction()));
        if (v.getId() == R.id.list){
            Log.i("ACTION_DOWN","ACTION_DOWN!!!");
            if (mEvent == MotionEvent.ACTION_DOWN || mEvent == MotionEvent.ACTION_MOVE){

                listView.requestDisallowInterceptTouchEvent(true);
            }else if (mEvent == MotionEvent.ACTION_CANCEL || mEvent == MotionEvent.ACTION_UP){
                listView.requestDisallowInterceptTouchEvent(false);
            }
        }
        return false;
    }
}
