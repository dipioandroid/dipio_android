package net.dipio.dipio;

import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.util.Pair;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.FrameLayout;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity implements ContactsFragment.OnFragmentInteractionListener, FavoriteFragment.OnFragmentInteractionListener, ChannelFragment.OnFragmentInteractionListener, SetupFragment.OnFragmentInteractionListener {

    private ContactsFragment contactsFragment;
    private FavoriteFragment favoriteFragment;
    private ChannelFragment channelFragment;
    private SetupFragment setupFragment;
    AudioManager audioManager;
    /*List<android.util.Pair<String, List<IContacts>>> list;*/

    List<IContacts> list;
    private static final String FAVORITE_NAME = "favoriteName";



    //TODO - service will have a getList() method


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FrameLayout frameLayout = (FrameLayout) findViewById(R.id.fragment_container);
        //adding the 1st fragment to screen - ContactsFragment
        contactsFragment = ContactsFragment.newInstance("","");
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().add(R.id.fragment_container, contactsFragment).commit();
        //controlling the stream for mic/volume setup
        setVolumeControlStream(AudioManager.STREAM_MUSIC);
        audioManager = (AudioManager) getSystemService(MainActivity.AUDIO_SERVICE);

        //TODO bind to the service and put friends and groups in a list
        //FOR TESTING ONLY-----------------------


/*        String[] titles = {"Friends", "Groups"};
        IContacts[][] contacts = {
                {
                        new PersonContact("id_1", "name_1", "dec_1", 1111, -1),
                        new PersonContact("id_2", "name_2", "dec_2", 1111, 0),
                        new PersonContact("id_3", "name_3", "dec_3", 1111, 1),
                        new PersonContact("id_4", "name_4", "dec_4", 1111, 2),
                        new PersonContact("id_5", "name_5", "dec_5", 1111, 3),
                        new PersonContact("id_6", "name_6", "dec_6", 1111, 4),
                        new PersonContact("id_7", "name_7", "dec_7", 1111, -1),
                        new PersonContact("id_8", "name_8", "dec_8", 1111, -1),
                        new PersonContact("id_9", "name_9", "dec_9", 1111, -1),
                        new PersonContact("id_10", "name_10", "dec_10", 1111, -1),
                        new PersonContact("id_11", "name_11", "dec_11", 1111, -1),
                        new PersonContact("id_12", "name_12", "dec_12", 1111, -1),
                        new PersonContact("id_13", "name_13", "dec_13", 1111, -1),
                        new PersonContact("id_14", "name_14", "dec_14", 1111, -1),
                        new PersonContact("id_15", "name_15", "dec_15", 1111, -1),
                },
                {
                        new GroupContact("id_1", "nameG_1", "dec_1", 1111, 4,2),
                        new GroupContact("id_2", "nameG_2", "dec_2", 1111, 1,3),
                        new GroupContact("id_3", "nameG_3", "dec_3", 1111, 2,4),
                        new GroupContact("id_4", "nameG_4", "dec_4", 1111, 3,2),
                        new GroupContact("id_5", "nameG_5", "dec_5", 1111, 5,2),
                        new GroupContact("id_6", "nameG_6", "dec_6", 1111, 6,2),
                },

        };*/
        list = new ArrayList<>();
        list.add(new PersonContact("id_1", "name_1", "dec_1", -1, 1));
        list.add(new PersonContact("id_2", "name_2", "dec_2", 0, 2));
        list.add(new PersonContact("id_3", "name_3", "dec_3", -1, 3));
        list.add(new GroupContact("id_1", "nameG_1", "dec_1", -1, 4,2));
        list.add(new GroupContact("id_2", "nameG_2", "dec_2", -2, 5,3));
        list.add(new PersonContact("id_1", "name_1", "dec_1", -1, 1));
        list.add(new PersonContact("id_2", "name_2", "dec_2", 0, 2));
        list.add(new PersonContact("id_3", "name_3", "dec_3", -1, 3));
        list.add(new GroupContact("id_1", "nameG_1", "dec_1", -1, 4,2));
        list.add(new GroupContact("id_2", "nameG_2", "dec_2", -2, 5,3));
        list.add(new PersonContact("id_1", "name_1", "dec_1", -1, 1));
        list.add(new PersonContact("id_2", "name_2", "dec_2", 0, 2));
        list.add(new PersonContact("id_3", "name_3", "dec_3", -1, 3));
        list.add(new GroupContact("id_1", "nameG_1", "dec_1", -1, 4,2));
        list.add(new GroupContact("id_2", "nameG_2", "dec_2", -2, 5,3));
        list.add(new PersonContact("id_1", "name_1", "dec_1", -1, 1));
        list.add(new PersonContact("id_2", "name_2", "dec_2", 0, 2));
        list.add(new PersonContact("id_3", "name_3", "dec_3", -1, 3));
        list.add(new GroupContact("id_1", "nameG_1", "dec_1", -1, 4,2));
        list.add(new GroupContact("id_2", "nameG_2", "dec_2", -2, 5,3));
            /*list.add(new android.util.Pair<String, List<IContacts>>(titles[0], Arrays.asList(contacts[0])));


            list.add(new android.util.Pair<String, List<IContacts>>(titles[1], Arrays.asList(contacts[1])));*/



        //---------------------------------------
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    public void moveFromFavoriteToChat(int position){
        //TODO - onclick move to new page (Activity or fragment)\
        //TODO - need to see what details of contact to bring so to open a chat
        Intent chatIntent = new Intent(MainActivity.this, ChatActivity.class);
        Bundle bundle =  new Bundle();
        //take from the service that holds the list of favorites the id and name of the contacts and
        //put in the bundle. then start activity chat
        startActivity(chatIntent);

    }





    public void switchFragment(int imageId){

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        switch (imageId){
            case R.id.imgv_contacts_bBar:
                contactsFragment = ContactsFragment.newInstance("", "");
                transaction.replace(R.id.fragment_container, contactsFragment, ConstantsDipio.CONTACTS_FRAGMENT_TAG);
                transaction.addToBackStack(ConstantsDipio.CONTACTS_FRAG_BACKSTACK);
                transaction.commit();
                break;
            case R.id.imgv_favorite_bBar:
                favoriteFragment = FavoriteFragment.newInstance("","");
                transaction.replace(R.id.fragment_container, favoriteFragment, ConstantsDipio.FAVORITE_FRAGMENT_TAG);
                transaction.addToBackStack(ConstantsDipio.FAVORITE_FRAG_BACKSTACK);
                transaction.commit();
                break;
            case R.id.imgv_channel_bBar:
                channelFragment = ChannelFragment.newInstance("","");
                transaction.replace(R.id.fragment_container, channelFragment, ConstantsDipio.CHANNEL_FRAGMENT_TAG);
                transaction.addToBackStack(ConstantsDipio.CHANNEL_FRAG_BACKSTACK);
                transaction.commit();
                break;
            case R.id.imgv_setup_bBar:
                setupFragment = SetupFragment.newInstance("","");
                transaction.replace(R.id.fragment_container, setupFragment, ConstantsDipio.SETUP_FRAGMENT_TAG);
                transaction.addToBackStack(ConstantsDipio.SETUP_FRAG_BACKSTACK);
                transaction.commit();
                break;

        }
    }

}
