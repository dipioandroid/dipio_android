package net.dipio.dipio;

import android.*;

/**
 * Created by qwerty on 04/08/16.
 */
public class ConstantsDipio {

    public static final String RETURN_CODE = "returnCode";
    public static final String ERROR_MSG = "errorMsg";
    public static final String SID = "sId";
    public static final String DSS_SVR = "dssSvr";

    public static final String CONTACTS_FRAGMENT_TAG = "contacts_tag";
    public static final String FAVORITE_FRAGMENT_TAG = "favorite_tag";
    public static final String CHANNEL_FRAGMENT_TAG = "channel_tag";
    public static final String SETUP_FRAGMENT_TAG = "setup_tag";
    public static final String MAP_FRAGMENT_TAG = "map_tag";
    public static final String PHOTO_FRAGMENT_TAG = "photo_tag";

    public static final String CONTACTS_FRAG_BACKSTACK = "contacts_backstack";
    public static final String FAVORITE_FRAG_BACKSTACK = "favorite_backstack";
    public static final String CHANNEL_FRAG_BACKSTACK = "channel_backstack";
    public static final String SETUP_FRAG_BACKSTACK = "setup_backstack";

    public static final String PHOTO_FRAG_BACKSTACK = "photo_backstack";

    public static final int CAMERA_REQUEST = 1;
    public static final int GALLERY_SELECT_IMAGE_REQUEST = 2;
    public static final int MY_PERMISSIONS_REQUEST_CAMERA_AND_STORAGE = 2;
    public static final String[] PERMISSION_STRING_CAMERA_AND_STORAGE = {android.Manifest.permission.CAMERA, android.Manifest.permission.WRITE_EXTERNAL_STORAGE};


}
