package net.dipio.dipio;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.v4.content.SharedPreferencesCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

public class SplashScreen extends AppCompatActivity implements Animation.AnimationListener {

    private ImageView imgvLogo;
    private static final int SPLASH_TIME_OUT = 6000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //check if user already logged in
        //check status in sharred preference
        SharedPreferences sharedPref = getSharedPreferences(getResources().getString(R.string.loginStatusFile), MODE_PRIVATE);
        boolean loginStatus = sharedPref.getBoolean(getString(R.string.preference_key_isLoggedIn), false);
        if (loginStatus){
            //user already logged in, go to contact page
            startActivity(new Intent(this,MainActivity.class));
        }else {
            //user not logged in
            setContentView(R.layout.activity_splash_screen);

            //2 sec delay in displaying Login page
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    //open Login activity after the splash time out ends
                    startActivity(new Intent(SplashScreen.this, LoginActivity.class));
                    overridePendingTransition(R.anim.push_up_in, R.anim.push_up_out);
                    //removing SplashActivity from the stack
                    finish();
                }
            }, SPLASH_TIME_OUT);

            imgvLogo = (ImageView) findViewById(R.id.imgv_dipio_splashScreen);
            Animation fadeInAnimation = AnimationUtils.loadAnimation(this, R.anim.fade_in_logo);
            fadeInAnimation.setAnimationListener(this);
            imgvLogo.startAnimation(fadeInAnimation);
        }

    }

    @Override
    public void onAnimationStart(Animation animation) {

    }

    //Start shaking animation
    @Override
    public void onAnimationEnd(Animation animation) {
        animation = AnimationUtils.loadAnimation(this,R.anim.shake_logo);
        imgvLogo.startAnimation(animation);
    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }
}
