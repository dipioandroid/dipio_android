package net.dipio.dipio;

/**
 * Created by qwerty on 03/08/16.
 */
public class UserDetails {

    private final String reqType = "login";
    private String scope;
    private String user;
    private String hashedPass;
    private String apiVersion;
    private String lang;
    private String phoneNum;

    public UserDetails(String scope, String user, String hashedPass, String apiVersion, String lang, String phoneNum){
        this.scope = scope;
        this.user = user;
        this.hashedPass = hashedPass;
        this.apiVersion = apiVersion;
        this.lang = lang;
        this.phoneNum = phoneNum;
    }
}
