package net.dipio.dipio;

public class GroupContact implements IContacts {

    public static final String TAG = GroupContact.class.getSimpleName();

    private String id;
    private String displayName;
    private String description;
    private long lastActive;
    private int isFavorite;
    private int numOfContacts;

    //TODO need to calculate if user is online based on "lastActive"
    private boolean isOnline;

    public GroupContact(String id, String displayName, String description, long lastActive, int isFavorite, int numOfContacts){
        this.id = id;
        this.displayName = displayName;
        this.description = description;
        this.lastActive = lastActive;
        this.isFavorite = isFavorite;
        this.numOfContacts = numOfContacts;
    }

    public boolean isOnline() {
        return isOnline;
    }

/*
    public void setOnline(boolean online) {
        isOnline = online;
    }
*/

    public int getNumOfContacts() {
        return numOfContacts;
    }

/*
    public void setNumOfContacts(int numOfContacts) {
        this.numOfContacts = numOfContacts;
    }
*/

    public int getIsFavorite() {
        return isFavorite;
    }

/*
    public void setIsFavorite(int isFavorite) {
        this.isFavorite = isFavorite;
    }
*/

    public long getLastActive() {
        return lastActive;
    }

/*
    public void setLastActive(long lastActive) {
        this.lastActive = lastActive;
    }
*/

    public String getDescription() {
        return description;
    }

/*
    public void setDescription(String description) {
        this.description = description;
    }
*/

    public String getId() {
        return id;
    }

/*
    public void setId(String id) {
        this.id = id;
    }
*/

    public String getDisplayName() {
        return displayName;
    }
/*
    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }*/
}

