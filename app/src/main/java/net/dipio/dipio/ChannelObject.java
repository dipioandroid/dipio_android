package net.dipio.dipio;

/**
 * Created by qwerty on 09/08/16.
 */
public class ChannelObject {

    private String channelId;
    private String[] contactOrGroupIds;
    private long lastMsgTime;
    private int numOfUnread;


    public ChannelObject(String channelId, String[] contactOrGroupIds , long lastMsgTime, int numOfUnread){
        this.channelId = channelId;
        this.contactOrGroupIds  = contactOrGroupIds;
        this.lastMsgTime = lastMsgTime;
        this.numOfUnread = numOfUnread;
    }

    public String getChannelId() {
        return channelId;
    }

    public String[] getContactOrGroupIds() {
        return contactOrGroupIds;
    }

    public long getLastMsgTime() {
        return lastMsgTime;
    }

    public int getNumOfUnread() {
        return numOfUnread;
    }
}
