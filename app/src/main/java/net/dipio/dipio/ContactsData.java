package net.dipio.dipio;

/**
 * Created by qwerty on 07/08/16.
 */

import android.util.Pair;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Class Pair belongs to Util library. it is used to pass 2 objects
 *  it has "first" and "second" fields representing the objects
 */
public class ContactsData {

    public static final String TAG = ContactsData.class.getSimpleName();

    public static List<Pair<String, List<IContacts>>> getAllData(){
        List<Pair<String, List<IContacts>>> res = new ArrayList<Pair<String, List<IContacts>>>();

        for (int i = 0; i < 2; i++) {
            res.add(getOneSection(i));
        }

        return res;
    }

    public static List<IContacts> getFlattenedData() {
        List<IContacts> res = new ArrayList<IContacts>();

        for (int i = 0; i < 2; i++) {
            res.addAll(getOneSection(i).second);
        }

        return res;
    }

    public static Pair<Boolean, List<IContacts>> getRows(int page) {
        List<IContacts> flattenedData = getFlattenedData();
        if (page == 1) {
            return new Pair<Boolean, List<IContacts>>(true, flattenedData.subList(0, 5));
        } else {
            /*SystemClock.sleep(2000); // simulate loading*/
            return new Pair<Boolean, List<IContacts>>(page * 5 < flattenedData.size(), flattenedData.subList((page - 1) * 5, Math.min(page * 5, flattenedData.size())));
        }
    }
    public static Pair<String, List<IContacts>> getOneSection(int index) {
        String[] titles = {"Friends", "Groups"};
        IContacts[][] contacts = {
                {
                        new PersonContact("id_1", "name_1", "dec_1", 1111, -1),
                        new PersonContact("id_2", "name_2", "dec_2", 1111, 0),
                        new PersonContact("id_3", "name_3", "dec_3", 1111, 1),
                        new PersonContact("id_4", "name_4", "dec_4", 1111, 2),
                        new PersonContact("id_5", "name_5", "dec_5", 1111, 3),
                        new PersonContact("id_6", "name_6", "dec_6", 1111, 4),
                        new PersonContact("id_7", "name_7", "dec_7", 1111, -1),
                        new PersonContact("id_8", "name_8", "dec_8", 1111, -1),
                        new PersonContact("id_9", "name_9", "dec_9", 1111, -1),
                        new PersonContact("id_10", "name_10", "dec_10", 1111, -1),
                        new PersonContact("id_11", "name_11", "dec_11", 1111, -1),
                        new PersonContact("id_12", "name_12", "dec_12", 1111, -1),
                        new PersonContact("id_13", "name_13", "dec_13", 1111, -1),
                        new PersonContact("id_14", "name_14", "dec_14", 1111, -1),
                        new PersonContact("id_15", "name_15", "dec_15", 1111, -1),
                },
                {
                        new GroupContact("id_1", "nameG_1", "dec_1", 1111, 4,2),
                        new GroupContact("id_2", "nameG_2", "dec_2", 1111, 1,3),
                        new GroupContact("id_3", "nameG_3", "dec_3", 1111, 2,4),
                        new GroupContact("id_4", "nameG_4", "dec_4", 1111, 3,2),
                        new GroupContact("id_5", "nameG_5", "dec_5", 1111, 5,2),
                        new GroupContact("id_6", "nameG_6", "dec_6", 1111, 6,2),
                },

        };
        return new Pair<String, List<IContacts>>(titles[index], Arrays.asList(contacts[index]));
    }

}

