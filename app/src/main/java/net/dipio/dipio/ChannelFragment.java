package net.dipio.dipio;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextSwitcher;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ChannelFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ChannelFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ChannelFragment extends Fragment {

    private RecyclerView mRecyclerView;
    private MyChannelAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private List<ChannelObject> channelList;
    private View v;
    private ImageView imgvFavorite;
    private ImageView imgvContacts;
    private ImageView imgvSetup;
    /*private EditText etSearch;*/
    private SearchView searchView;
    private static int TYPING_FLAG = 0;


    /*list for debugging, need to replace later*/

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public ChannelFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ChannelFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ChannelFragment newInstance(String param1, String param2) {
        ChannelFragment fragment = new ChannelFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_channel, container, false);

        imgvContacts = (ImageView) v.findViewById(R.id.imgv_contacts_bBar);
        imgvFavorite = (ImageView) v.findViewById(R.id.imgv_favorite_bBar);
        imgvSetup = (ImageView) v.findViewById(R.id.imgv_setup_bBar);
        imgvContacts.setOnClickListener(buttomBarListener);
        imgvFavorite.setOnClickListener(buttomBarListener);
        imgvSetup.setOnClickListener(buttomBarListener);
        /*etSearch = (EditText) v.findViewById(R.id.et_search);*/
        searchView = (SearchView) v.findViewById(R.id.searchview_channel);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                Log.i("onQueryTextSubmit", query);

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                Log.i("textQuery", String.valueOf(newText.length()));
                    mAdapter.filter(newText);

                return true;
            }
        });



  /*      etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mAdapter.getFilter().filter(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });*/

        mRecyclerView = (RecyclerView) v.findViewById(R.id.recyclerView_channel);
        mRecyclerView.setHasFixedSize(true);




        //TODO - need to populate the list from server before creating the adapter
        //remove this list setup
        channelList = new ArrayList<>();
        channelList.add(new ChannelObject("user 1", new String[]{"contact1"}, -1, 0));
        channelList.add(new ChannelObject("user 2", new String[]{"contact1"}, -1, 1));
        channelList.add(new ChannelObject("user 3", new String[]{"contact1"}, -1, 2));
        channelList.add(new ChannelObject("user 4", new String[]{"contact1"}, -1, 3));
        channelList.add(new ChannelObject("user 5", new String[]{"contact1"}, -1, 4));
        channelList.add(new ChannelObject("user 6", new String[]{"contact1"}, -1, 11));
        channelList.add(new ChannelObject("group 1", new String[]{"contact1", "contact2"}, -1, 1));
        channelList.add(new ChannelObject("group 2", new String[]{"contact1", "contact2"}, -1, 0));
        channelList.add(new ChannelObject("group 3", new String[]{"contact1", "contact2"}, -1, 14));
        channelList.add(new ChannelObject("group 4", new String[]{"contact1", "contact2"}, 10, 0));
        channelList.add(new ChannelObject("group 5", new String[]{"contact1", "contact2"}, 11, 15));
        channelList.add(new ChannelObject("group 6", new String[]{"contact1", "contact2"}, 12, 9));



        return v;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new MyChannelAdapter(getActivity(), channelList);
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void switchFragment(int imageId);
        void onFragmentInteraction(Uri uri);
    }

    View.OnClickListener buttomBarListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            mListener.switchFragment(v.getId());
        }
    };




}
