package net.dipio.dipio;

import android.content.Context;
import android.content.res.Resources;
import android.media.AudioManager;
import android.media.AudioRecord;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.print.PrintHelper;
import android.support.v7.widget.SwitchCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.SoundEffectConstants;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.TextView;

import java.io.IOException;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link SetupFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link SetupFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SetupFragment extends Fragment {

    private static boolean EDIT_PROFILE_ARROW_OPEN = false;
    private static boolean EDIT_NOTIFICATIONS_ARROW_OPEN = false;


    private View v;
    private ImageView editProfileExtensionArrow;
    private ImageView editNotificationsExtensionArrow;
    private  FrameLayout includedFrameLayoutEditProfile;
    private FrameLayout includedFrameLayoutEditNotifications;
    private RadioGroup radioGroup;

    private ImageView imgvContact;
    private ImageView imgvChannel;
    private ImageView imgvFavorite;

    private TextView tvCompany;
    private TextView tvUser;
    private EditText etPassword;
    private EditText etAlias;
    private Button btnSave;

    private SwitchCompat switchAutoLogin;
    private SwitchCompat switchServerLossNotification;
    private SwitchCompat switchSendLocation;
    private SwitchCompat switchShowMsg;

    private TextView tryVolume;
    private TextView tryMic;
    private SeekBar seekBarVolume;
    private SeekBar seekBarMic;

    private MediaPlayer mediaPlayer;
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public SetupFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment SetupFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static SetupFragment newInstance(String param1, String param2) {
        SetupFragment fragment = new SetupFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_setup, container, false);

        editProfileExtensionArrow = (ImageView) v.findViewById(R.id.editImgv_closed_arrow);
        editNotificationsExtensionArrow = (ImageView) v.findViewById(R.id.notificationsImgv_closed_arrow);
        editProfileExtensionArrow.setOnClickListener(extensionArrowListener);
        editNotificationsExtensionArrow.setOnClickListener(extensionArrowListener);
        editNotificationsExtensionArrow = (ImageView) v.findViewById(R.id.notificationsImgv_closed_arrow);
        includedFrameLayoutEditProfile = (FrameLayout) v.findViewById(R.id.includedLayout_edit_profile);
        includedFrameLayoutEditNotifications = (FrameLayout) v.findViewById(R.id.includedLayout_edit_notifications);

        imgvContact = (ImageView) v.findViewById(R.id.imgv_contacts_bBar);
        imgvFavorite = (ImageView) v.findViewById(R.id.imgv_favorite_bBar);
        imgvChannel = (ImageView) v.findViewById(R.id.imgv_channel_bBar);
        imgvContact.setOnClickListener(buttomBarListener);
        imgvFavorite.setOnClickListener(buttomBarListener);
        imgvChannel.setOnClickListener(buttomBarListener);

        tvCompany = (TextView) v.findViewById(R.id.tv_editEx_company);
        tvUser = (TextView) v.findViewById(R.id.tv_editEx_user);
        etPassword = (EditText) v.findViewById(R.id.et_editEx_password);
        etAlias = (EditText) v.findViewById(R.id.et_editEx_alias);
        btnSave = (Button) v.findViewById(R.id.btn_save_setupFragment);
        btnSave.setOnClickListener(btnSaveListener);

        radioGroup = (RadioGroup) v.findViewById(R.id.edit_radioGroup);
        radioGroup.setOnCheckedChangeListener(radioGroupListener);

        switchAutoLogin = (SwitchCompat) v.findViewById(R.id.autoLogin_swicher);
        switchServerLossNotification = (SwitchCompat) v.findViewById(R.id.serverLoss_switcher);
        switchSendLocation = (SwitchCompat) v.findViewById(R.id.sendLocation_switcher);
        switchShowMsg = (SwitchCompat) v.findViewById(R.id.showMsg_switcher);

        switchAutoLogin.setOnCheckedChangeListener(switchBtnListener);
        switchServerLossNotification.setOnCheckedChangeListener(switchBtnListener);
        switchSendLocation.setOnCheckedChangeListener(switchBtnListener);
        switchShowMsg.setOnCheckedChangeListener(switchBtnListener);

        tryVolume = (TextView) v.findViewById(R.id.volume_tryBtn);
        tryMic = (TextView) v.findViewById(R.id.mic_tryBtn);
        seekBarVolume = (SeekBar) v.findViewById(R.id.volume_seekBar);
        seekBarMic = (SeekBar) v.findViewById(R.id.mic_seekBar);
        tryVolume.setOnClickListener(tryBtnListener);
        tryMic.setOnClickListener(tryBtnListener);

        settingSeekBars();
    /*    seekBarVolume.setOnSeekBarChangeListener(seekBarListener);
        seekBarMic.setOnSeekBarChangeListener(seekBarListener);*/


        return v;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        includedFrameLayoutEditProfile.setVisibility(View.GONE);
        includedFrameLayoutEditNotifications.setVisibility(View.GONE);

    }

    @Override
    public void onStart() {
        super.onStart();
        mediaPlayer = MediaPlayer.create(getActivity(), R.raw.finish_beep_flac);

    }

    @Override
    public void onStop() {
        super.onStop();
        mediaPlayer.release();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
        void switchFragment(int imageId);
    }

    View.OnClickListener extensionArrowListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.editImgv_closed_arrow:

                    if (EDIT_PROFILE_ARROW_OPEN){
                        //drawer open, hide it and change arrow
                        includedFrameLayoutEditProfile.setVisibility(View.GONE);
                        editProfileExtensionArrow.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.arrow_close, null));
                        EDIT_PROFILE_ARROW_OPEN = false;
                    } else {
                        includedFrameLayoutEditProfile.setVisibility(View.VISIBLE);
                        editProfileExtensionArrow.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.arrow_open, null));
                        EDIT_PROFILE_ARROW_OPEN = true;

                        //TODO - take data from server and show in the editTexts views

                    }
                    break;
                case R.id.notificationsImgv_closed_arrow:
                    if (EDIT_NOTIFICATIONS_ARROW_OPEN){
                        //drawer open, hide it and change arrow
                        includedFrameLayoutEditNotifications.setVisibility(View.GONE);
                        editNotificationsExtensionArrow.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.arrow_close, null));
                        EDIT_NOTIFICATIONS_ARROW_OPEN = false;
                    }else {
                        includedFrameLayoutEditNotifications.setVisibility(View.VISIBLE);
                        editNotificationsExtensionArrow.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.arrow_open, null));
                        EDIT_NOTIFICATIONS_ARROW_OPEN = true;

                        //TODO - bring from server the current notification setup and set the radio button accordingly
                    }


                    break;
            }
        }
    };


    View.OnClickListener buttomBarListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            mListener.switchFragment(v.getId());
        }
    };

    RadioGroup.OnCheckedChangeListener radioGroupListener = new RadioGroup.OnCheckedChangeListener(){

        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            //TODO - save choosen option to server
            //drawer open, hide it and change arrow
            includedFrameLayoutEditNotifications.setVisibility(View.GONE);
            editNotificationsExtensionArrow.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.arrow_close, null));
            EDIT_NOTIFICATIONS_ARROW_OPEN = false;

        }
    };

    Button.OnClickListener btnSaveListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            String  pass = etPassword.getText().toString();
            String alias = etAlias.getText().toString();
            if (!TextUtils.isEmpty(pass) && !TextUtils.isEmpty(alias)){
                //TODO - save new settings to server - both fields
            }else if (!TextUtils.isEmpty(pass) && TextUtils.isEmpty(alias)){
                //save pass only
                //TODO - save new settings to server - pass only

            }else if (TextUtils.isEmpty(pass) && !TextUtils.isEmpty(alias)){
                //save alias only
                //TODO - save new settings to server - alias only
            }

            //close drawer
            includedFrameLayoutEditProfile.setVisibility(View.GONE);
            editProfileExtensionArrow.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.arrow_close, null));
            EDIT_PROFILE_ARROW_OPEN = false;
        }
    };

    SwitchCompat.OnCheckedChangeListener switchBtnListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            switch (buttonView.getId()){
                case R.id.autoLogin_swicher:
                    setSwitchBackground(isChecked, buttonView);

                    //TODO - save the isChecked status to server
                    break;
                case R.id.serverLoss_switcher:
                    setSwitchBackground(isChecked, buttonView);

                    //TODO - save the isChecked status to server
                    break;
                case R.id.sendLocation_switcher:
                    setSwitchBackground(isChecked, buttonView);

                    //TODO - save the isChecked status to server
                    break;
                case R.id.showMsg_switcher:
                    setSwitchBackground(isChecked, buttonView);
                    //TODO - save the isChecked status to server
                    break;
            }

        }
    };


    private void setSwitchBackground(boolean isChecked, CompoundButton button){
        if (isChecked){
            button.setBackground(getResources().getDrawable(R.drawable.border_blue, null));
        }else {
            button.setBackground(getResources().getDrawable(R.drawable.border_grey, null));
        }
    }

    private void settingSeekBars(){
        seekBarVolume.setOnSeekBarChangeListener(seekBarListener);
        seekBarMic.setOnSeekBarChangeListener(seekBarListener);

        seekBarVolume.setMax(((MainActivity)getActivity()).audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC));
        seekBarVolume.setProgress(((MainActivity)getActivity()).audioManager.getStreamVolume(AudioManager.STREAM_MUSIC));
    }

    SeekBar.OnSeekBarChangeListener seekBarListener = new SeekBar.OnSeekBarChangeListener() {
        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {


        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {
            int seekBarId = seekBar.getId();
            ((MainActivity)getActivity()).audioManager.setStreamVolume(AudioManager.STREAM_MUSIC,seekBar.getProgress(), 0);
        }
    };

    View.OnClickListener tryBtnListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            mediaPlayer.start();

            MediaRecorder mediaRecorder = new MediaRecorder();



        }
    };
}
