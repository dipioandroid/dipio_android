package net.dipio.dipio;

import android.*;
import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by qwerty on 11/08/16.
 */
public class PicturesAndGallery {


    private Context context;
    private String mCurrentPhotoPath;
    public PicturesAndGallery(Context context) {

        this.context = context;
    }

    public void openCamera(){
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            requestCameraAndStoragePermissions();
        }else {
            openCameraView();
        }
    }

    private void openCameraView(){
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (cameraIntent.resolveActivity(context.getPackageManager()) != null){
            // Create the File where the photo should be saved
            File photoFile = null;
            if (isExternalStorageWritable()){
                try {
                    photoFile = createImageFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                // Continue only if the File was successfully created
                if (photoFile != null){
                    Log.i("@@@@@@@","$$$$$$$$$$$");
                    cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
                    ((ChatActivity)context).startActivityForResult(cameraIntent, ConstantsDipio.CAMERA_REQUEST);
                }
            }else {
                Log.i(String.valueOf(R.string.external_storage_not_mounted), String.valueOf(R.string.external_storage_not_mounted));
            }

        }
    }


    public void requestCameraAndStoragePermissions() {
        if (ActivityCompat.shouldShowRequestPermissionRationale((ChatActivity)context, Manifest.permission.CAMERA)
                || ActivityCompat.shouldShowRequestPermissionRationale((ChatActivity)context, Manifest.permission.WRITE_EXTERNAL_STORAGE)){
         /*   Snackbar.make(context.relativeLayoutchat, R.string.permission_camera_and_storage, Snackbar.LENGTH_INDEFINITE).setAction(R.string.ok, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ActivityCompat.requestPermissions((ChatActivity)context, ConstantsDipio.PERMISSION_STRING_CAMERA_AND_STORAGE, ConstantsDipio.MY_PERMISSIONS_REQUEST_CAMERA_AND_STORAGE);
                }
            }).show();*/
            ((ChatActivity)context).showSnackBar();
        }else {
            // Contact permissions have not been granted yet. Request them directly.
            ActivityCompat.requestPermissions((ChatActivity)context, ConstantsDipio.PERMISSION_STRING_CAMERA_AND_STORAGE, ConstantsDipio.MY_PERMISSIONS_REQUEST_CAMERA_AND_STORAGE);
        }


/*

        if (ContextCompat.checkSelfPermission(context, android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions((ChatActivity)context, ConstantsDipio.PERMISSION_STRING_CAMERA_AND_STORAGE, ConstantsDipio.MY_PERMISSIONS_REQUEST_CAMERA_AND_STORAGE);

        }else {
            Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            if (cameraIntent.resolveActivity(context.getPackageManager()) != null){
                // Create the File where the photo should be saved
                File photoFile = null;
                if (isExternalStorageWritable()){
                    try {
                        photoFile = createImageFile();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    // Continue only if the File was successfully created
                    if (photoFile != null){
                        Log.i("@@@@@@@","$$$$$$$$$$$");
                        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
                        ((ChatActivity)context).startActivityForResult(cameraIntent, ConstantsDipio.CAMERA_REQUEST);
                    }
                }else {
                    Log.i(String.valueOf(R.string.external_storage_not_mounted), String.valueOf(R.string.external_storage_not_mounted));
                }

            }
        }

*/



    }



    private boolean isExternalStorageWritable(){
        if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())){
            //media storage is mounted
            return true;
        }
        return false;
    }

/*    private File createImageFile() throws IOException {
        String timeStamp = new SimpleDateFormat("yyyy_MMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);

*//*        new File(storageDir + "/" + R.string.directory_for_photos_saving);
        File outputfile=new File(storageDir + "/" + R.string.directory_for_photos_saving, imageFileName);
        mCurrentPhotoPath = "file: " + outputfile.getAbsolutePath();*//*

        File image = File.createTempFile(imageFileName, ".jpg", storageDir);
        mCurrentPhotoPath = "file: " + image.getAbsolutePath();

        return image;
*//*
        return outputfile;
*//*

    }*/

    private File createImageFile() throws IOException {
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES),"DipioMedia");
        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()){
            if (!mediaStorageDir.mkdirs()){
                //failed to create directory
                return null;
            }
        }
        java.util.Date date= new java.util.Date();
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(date.getTime());
        File mediaFile;
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + "DipioMedia" + timeStamp + ".jpg");
        mCurrentPhotoPath = "file: " + mediaFile.getAbsolutePath();
        Log.i("!!", mCurrentPhotoPath);
        return mediaFile;
    }


    //adding photo to gallery
    public void galleryAddPic(){
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File f = new File(mCurrentPhotoPath);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        context.sendBroadcast(mediaScanIntent);
    }

    public String getmCurrentPhotoPath(){
        return mCurrentPhotoPath;
    }

    public void openGallery(){
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        ((ChatActivity)context).startActivityForResult(Intent.createChooser(intent, "Select Picture"),ConstantsDipio.GALLERY_SELECT_IMAGE_REQUEST);
    }
}