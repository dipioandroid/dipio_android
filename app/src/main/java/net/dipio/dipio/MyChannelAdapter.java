package net.dipio.dipio;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by qwerty on 09/08/16.
 */
public class MyChannelAdapter extends RecyclerView.Adapter<MyChannelAdapter.ViewHolder>{

    private Context context;
    private List<ChannelObject> channelList;
    private List<ChannelObject> listScreenShot;


    public MyChannelAdapter(Context context, List<ChannelObject> channelList){
        this.context = context;
        this.channelList = channelList;
        listScreenShot = new ArrayList<>(channelList);
    }




    public static class ViewHolder extends RecyclerView.ViewHolder  implements View.OnClickListener{
        public ImageView imgvUseOrGroup;
        public TextView tvUserOrGroupName;
        public ImageView imgvBubble;
        public ImageView imgvPtt;
        public View linearLayout;
        public TextView tvTime;
        public TextView tvNumofUnReadMsg;
        public IMyViewHolderClicks iClickListener;


        public ViewHolder(View v, IMyViewHolderClicks iClickListener){
            super(v);
            this.iClickListener = iClickListener;
            imgvUseOrGroup = (ImageView) v.findViewById(R.id.imgv_user_channel);
            tvUserOrGroupName = (TextView) v.findViewById(R.id.tv_contactName_channel);
            imgvBubble = (ImageView) v.findViewById(R.id.imgv_bubble_channel);
            imgvPtt = (ImageView) v.findViewById(R.id.imgv_ptt_channel);
            linearLayout = (LinearLayout) v.findViewById(R.id.linearLayout_channel);
            tvTime = (TextView) v.findViewById(R.id.tv_time_channel);
            tvNumofUnReadMsg = (TextView) v.findViewById(R.id.tv_num_unread_msg);
            imgvPtt.setOnClickListener(this);  //TODO - does ptt need to be long press and hold?
            v.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            //Doesnt matter what view I clicked in an item,
            //i will move to the chat page of this group/friend unless its ptt
            //TODO - see what info need to send to an MainActivity method so to
            //TODO - move to the chat page
            if (v.getId() == R.id.imgv_ptt_channel){
                iClickListener.startPtt();
            }else {
                iClickListener.moveToChatPage();
            }

        }

        public static interface IMyViewHolderClicks{
            public void moveToChatPage();
            public void startPtt();
        }
    }

    @Override
    public MyChannelAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.channel_list_item, parent, false);
        MyChannelAdapter.ViewHolder vh = new ViewHolder(v, new ViewHolder.IMyViewHolderClicks() {
            @Override
            public void moveToChatPage() {
                //TODO - apply logic of data send
                context.startActivity(new Intent(context, ChatActivity.class));
                /*context.startActivity(new Intent(context, MapsActivity.class));*/
                Log.d("MOVE TO CHAT!!!", "MOVE TO CHAT!!!");
            }

            @Override
            public void startPtt() {
                Log.d("Start PTT!!!", "Start PTT!!!");
            }
        });
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        holder.tvUserOrGroupName.setText(channelList.get(position).getChannelId());
        int unRead = channelList.get(position).getNumOfUnread();
        long lastMsgTime = channelList.get(position).getLastMsgTime();

        if (channelList.get(position).getContactOrGroupIds().length > 1){
            //this is a group
            holder.imgvUseOrGroup.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_group_channel, null));
            //TODO - need to get the time from the server as is and set the TextView (take it from the list)
            if (unRead > 0){
                holder.tvNumofUnReadMsg.setText(String.valueOf(unRead));
            }else {
                holder.tvNumofUnReadMsg.setText(null);
            }

            if (lastMsgTime == -1){
                holder.tvNumofUnReadMsg.setBackground(context.getDrawable(R.drawable.green_filled_circle));
            }else {
                holder.tvNumofUnReadMsg.setBackground(context.getDrawable(R.drawable.green_empty_circle));

            }
        }else {
            //this is a single user
            holder.imgvUseOrGroup.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_user_channel, null));
            //TODO - need to get the time from the server as is and set the TextView (take it from the list)
        }
    }

    @Override
    public int getItemCount() {
        return channelList.size();
    }


    public void filter(String text){
        if (text.isEmpty()){
            channelList.clear();
            channelList.addAll(listScreenShot);
        }else {
            List<ChannelObject> result = new ArrayList<>();
            text = text.toLowerCase();
            for (ChannelObject item: listScreenShot){
                if (item.getChannelId().toLowerCase().contains(text)){
                    result.add(item);
                }
            }
            channelList.clear();
            channelList.addAll(result);
        }
        Log.i("copy size!!!", String.valueOf(listScreenShot.size()));
        notifyDataSetChanged();
    }


}
