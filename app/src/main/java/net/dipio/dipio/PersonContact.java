package net.dipio.dipio;

/**
 * Created by qwerty on 07/08/16.
 */
public class PersonContact implements IContacts {

    public static final String TAG = PersonContact.class.getSimpleName();

    private String id;
    private String displayName;
    private String description;
    private long lastSeen;
    private int isFavorite;

    //TODO need to calculate if user is online based on "lastSeen"
    private boolean isOnline;

    public PersonContact(String id, String displayName, String description, long lastSeen, int isFavorite){
        this.id = id;
        this.displayName = displayName;
        this.description = description;
        this.lastSeen = lastSeen;
        this.isFavorite = isFavorite;
    }

    public String getId() {
        return id;
    }

/*
    public void setId(String id) {
        this.id = id;
    }
*/

    public String getDisplayName() {
        return displayName;
    }

/*
    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }
*/

    public String getDescription() {
        return description;
    }

/*
    public void setDescription(String description) {
        this.description = description;
    }
*/

    public long getLastSeen() {
        return lastSeen;
    }

/*
    public void setLastSeen(long lastSeen) {
        this.lastSeen = lastSeen;
    }
*/

    public int getIsFavorite() {
        return isFavorite;
    }

/*
    public void setIsFavorite(int isFavorite) {
        this.isFavorite = isFavorite;
    }
*/

    public boolean isOnline() {
        return isOnline;
    }

/*
    public void setOnline(boolean online) {
        isOnline = online;
    }
*/
}

